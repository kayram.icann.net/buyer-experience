module.exports = {
  ci: {
    collect: {
      numberOfRuns: 5,
      startServerCommand: 'yarn start',
      url: ['http://localhost:3000/', 'http://localhost:3000/pricing/'],
      settings: {
        onlyCategories: [
          'performance',
          'accessibility',
          'best-practices',
          'seo',
        ],
        skipAudits: ['uses-http2'],
        chromeFlags: '--no-sandbox',
        preset: 'desktop',
      },
    },
    upload: {
      target: 'temporary-public-storage',
    },
  },
};
