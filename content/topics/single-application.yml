---
  title: Why a single application for DevOps?
  description: Learn how simplifying your toolchain using a single application can increase your speed while reducing cost and risk.
  topics_breadcrumb: true
  topic_name: Single application
  icon: monitor-test
  topics_header:
    data:
      title: Why a single application for DevOps?
      updated_date:
      block:
        - text: |
            Learn how simplifying your toolchain using a single application can increase your speed while reducing cost and risk.
          link_text: "Try GitLab for free"
          link_href: /free-trial
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Single application
  side_menu:
    anchors:
      text: "On this page"
      data:
      - text: The DevOps toolchain tax
        href: "#the-dev-ops-toolchain-tax"
        data_ga_name: the devops toolchain tax
        data_ga_location: side-navigation

      - text: Single application architecture
        href: "#single-application-architecture"
        data_ga_name: single application architecture
        data_ga_location: side-navigation

      - text: Benefits of a single application
        href: "#benefits-of-a-single-application"
        data_ga_name: benefits of a single application
        data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []       
    content:
      - name: topics-copy-block
        data:
          header: The DevOps toolchain tax
          column_size: 10
          blocks:
            - text: | 
                Traditional DevOps solutions require cobbling together multiple tools that were never designed to work together in order to build an DevOps toolchain.
              image:
                image_url: "/nuxt-images/topics/devops-toolchain-complexity.png"
                alt: "A web of competitor logos showing the complexity of the DevOps toolchain. A block of text explains that the problems with using too many tools include: keeping up with the competition, understanding performance and business drivers, it's difficult to improve efficiency, and it is difficult to protect businesses from security threat. Another block of text shares that a list of solutions that include: launching new features, expanding product offerings and rolling-out upgrades, utilizing data insights to benchmark performance and assess peers, automate processes, and monitoring, discovering, and addressing security breaches"
            - text: |
                This leads to having to pay a “tax” on your toolchain made up hidden costs.

                - Time and cost to acquire point tools

                - Time and cost to integrate all of these tools

                - Time and cost to train users on many tools

                - User context switching between all of these tools

                - Time and cost to administer all of these tools
      - name: topics-copy-block
        data:
          header: Single application architecture
          column_size: 10
          blocks:
            - text: | 
                GitLab is a complete DevOps platform designed from the ground up as a single application. From project planning and source code management, to CI/CD, security, and monitoring GitLab’s capabilities are built-in as part of the app so you don’t have to integrate multiple tools.
      - name: topics-copy-block
        data:
          header: Benefits of a single application
          column_size: 10
          blocks:
            - video:
                video_url: https://www.youtube.com/embed/MNxkyLrA5Aw
              
              text: | 
                ### Reduce risk with a single set of permissions
                Rather than having to manage authentication and authorization across many tools. GitLab has a single login and one place to set permissions so everyone has the correct access.

                ### Reduce costs with less administrative overhead
                With a single application to install, configure, and maintain there’s less administrative overhead. Since fewer staff needed to administer a single application verse a complex toolchain more of your engineering resources can be allocated towards development of features for your users.

                ### Increase speed with a lower time to resolution
                When a build pipeline fails how do you troubleshoot? Is it a problem with the infrastructure or did new code fail a test? Perhaps there is state in the original specification that needed to help debug. With traditional toolchains the issue tracker, code repository, and CI/CD pipeline are all separate tools. When teams need to troubleshoot they have to pass state back and forth in a ticket because they likely don’t all have access to the same applications.

                With GitLab, everyone who needs to help troubleshoot a failure has access to all of the data. Pipeline, code, comments, issues, and test results all appear on the merge request so there’s a single view. With everyone on the same page troubleshooting is much simpler and things get up and running faster.

                [See more benefits of a single application](https://about.gitlab.com/handbook/product/single-application/){data-ga-name: single application benefits, data-ga-location: body}


                