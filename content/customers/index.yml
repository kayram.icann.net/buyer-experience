---
title: Case studies from GitLab customers
description: Customers share how they've been able to shorten the software development lifecycle while using GitLab
hero:
  note: Customer stories
  header: Customers ship software faster with GitLab
  video:
    url: https://www.youtube.com/embed/sT85nT9X2mM
    photourl: /nuxt-images/customers/nasdaq-hero.png
  # metrics:
  #   - value: 50%+
  #     text: of the Fortune 100
  #   - value: 30m+
  #     text: Active users
customer_showcase:
  header: From planning to production, see how GitLab brings teams together
  categories:
    - name: 'Featured'
      cta:
        text: See all customer stories
        href: /customers/all/
      quote:
        image: /nuxt-images/blogimages/airbus_cover_image.jpg
        logo: /nuxt-images/software-faster/airbus-logo.png
        text: “It’s simple. All teams operate around this one tool. Instantly, that made communication easier. We wouldn’t be where we are today if we didn’t have GitLab in our stack.”
        author:
          name: Logan Weber
          title: Software Automation Engineer, Airbus Defence and Space, Intelligence
        button:
          href: /customers/airbus/
      cases:
        - image: /nuxt-images/blogimages/dunelm.png
          logo: /nuxt-images/logos/dunelm.svg
          text: How Dunelm shifted security left and streamlined collaboration with GitLab
          button:
            href: /customers/dunelm/
        - image: /nuxt-images/blogimages/hackerone-cover-photo.jpg
          logo: /nuxt-images/logos/hackerone-logo.png
          text: HackerOne achieves 5x faster deployments with GitLab’s integrated security
          button:
            href: /customers/hackerone/
    - name: 'Technology'
      cta:
        text: See more Technology stories
        href: /customers/all/?industry=technology
      quote:
        image: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
        logo: /nuxt-images/home/logo_siemens_mono.svg
        text: “We really try to bring the open source culture in, and so far, we really succeeded. With CI/CD, we have one and a half million builds every month. The whole culture has completely changed.”
        author:
          name: Fabio Huser
          title: Software Architect at Siemens Smart Infrastructure
        button:
          href: /customers/siemens/
      cases:
        - image: /nuxt-images/blogimages/nvidia.jpg
          logo: /nuxt-images/logos/nvidia-logo.svg
          text: GitLab Geo helps NVIDIA’s development teams to stay secure and highly communicative
          button:
            href: /customers/Nvidia/
        - image: /nuxt-images/blogimages/iron-mountain-2.png
          logo: /nuxt-images/home/logo_iron_mountain_mono.svg
          text: Iron Mountain drives DevOps evolution with GitLab Ultimate
          button:
            href: /customers/iron-mountain/
    - name: 'Financial Services'
      cta:
        text: See more Financial services stories
        href: /customers/all/?industry=financial-services
      quote:
        image: /nuxt-images/blogimages/Goldman_Sachs_case_study.jpg
        logo: /nuxt-images/enterprise/logo-goldman-sachs.svg
        text: “GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs.”
        author:
          name: Andrew Knight
          title: Managing Director, Goldman Sachs
        button:
          href: /customers/goldman-sachs/
      cases:
        - image: /nuxt-images/blogimages/bab_cover_image.jpg
          logo: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
          text: How GitLab is accelerating DevOps at Bendigo and Adelaide Bank
          button:
            href: /customers/bab/
        - image: /nuxt-images/blogimages/keytradebank_cover.jpg
          logo: /nuxt-images/logos/keytrade-bank.svg
          text: Keytrade Bank centralizes its tooling around GitLab
          button:
            href: /customers/keytradebank/
    - name: 'Education'
      cta:
        text: See more Education stories
        href: /customers/all/?industry=education
      quote:
        image: /nuxt-images/blogimages/deakin-university.jpg
        logo: /nuxt-images/logos/deakin-logo-1.png
        text: “One of the drivers for us to adopt GitLab was the number of different out-of-the-box security features that allowed us to replace other solutions and open source tools and therefore the skill sets that came along with them.”
        author:
          name: Aaron Whitehand
          title: Director of Digital Enablement, Deakin University
        button:
          href: /customers/deakin-university/
      cases:
        - image: /nuxt-images/solutions/education/university_of_surrey.jpg
          logo: /nuxt-images/case-study-logos/logo_uni_surrey.svg
          text: The University of Surrey achieves top marks for collaboration and workflow management with GitLab
          button:
            href: /customers/university-of-surrey/
        - image: /nuxt-images/solutions/education/victoria.jpeg
          logo: /nuxt-images/logos/victoria-university-wellington-logo.svg
          text: How GitLab advances open science education at New Zealand’s top-ranked research university
          button:
            href: /customers/victoria_university/
logo_grid:
  paginated: true
  rows_per_page: 3
  partners:
    - logo: /nuxt-images/software-faster/airbus-logo.png
      alt: airbus logo
      href: /customers/airbus/
    - logo: /nuxt-images/logos/hackerone-logo.png
      alt: hackerone logo
      href: /customers/hackerone/
    - logo: /nuxt-images/enterprise/logo-nasdaq.svg
      alt: Nasdaq logo
      href: https://www.youtube.com/watch?v=sT85nT9X2mM
    - logo: /nuxt-images/home/logo_ubs_mono.svg
      alt: UBS Logo
      href: /blog/2021/08/04/ubs-gitlab-devops-platform/
    - logo: /nuxt-images/case-study-logos/siemens-logo.png
      alt: siemens logo
      href: /customers/siemens/
    - logo: /nuxt-images/logos/hilti_logo_1.png
      alt: hilti logo
      href: /customers/hilti/
    - logo: /nuxt-images/case-study-logos/FujitsuCTL_Logo.png
      alt: fujitsu logo
      href: /customers/fujitsu/
    - logo: /nuxt-images/customers/Fanatics_logo2.svg
      alt: fanatics logo
      href: /customers/fanatics/
    - logo: /nuxt-images/logos/haven-tech-logo.png
      alt: haven technologies logo
      href: /customers/haven-technologies/
    - logo: /nuxt-images/logos/deakin-logo-1.png
      alt: deakin university logo
      href: /customers/deakin-university/
    - logo: /nuxt-images/logos/dunelm.svg
      alt: Dunelm logo
      href: /customers/dunelm/
    - logo: /nuxt-images/logos/nvidia-logo.svg
      alt: nvidia logo
      href: /customers/Nvidia/
    - logo: /nuxt-images/enterprise/logo-goldman-sachs.svg
      alt: Goldman Sachs logo
      href: /customers/goldman-sachs/
    - logo: /nuxt-images/home/im_logo_final_color.png
      alt: iron mountain logo
      href: /customers/iron-mountain/
    - logo: /nuxt-images/customers/credit-agricole-logo-1.png
      alt: credit agricole logo
      href: /customers/credit-agricole/
company_showcase:
  header: Organizations of all sizes trust GitLab
  categories:
    - name: 'Enterprise'
      cta:
        text: See more Enterprise stories
        href: /customers/all/?companySize=enterprise
      cases:
        - image: /nuxt-images/blogimages/bab_cover_image.jpg
          name: Bendigo and Adelaide Bank
          text: Accelerating DevOps with GitLab
          button:
            href: /customers/bab/
        - image: /nuxt-images/blogimages/nvidia.jpg
          name: Nvidia
          text: How GitLab Geo supports NVIDIA’s innovation
          button:
            href: /customers/conversica/
        - image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
          name: Hemmersbach
          text: Increasing build speed by 59x with GitLab
          button:
            href: /customers/hemmersbach/
      quote:
        logo: /nuxt-images/logos/hilti_logo_1.png
        header: How CI/CD and robust security scanning accelerated Hilti’s SDLC
        text: “GitLab is bundled together like a suite and then ships with a very sophisticated installer. And then it somehow works. This is very nice if you're a company which just wants to get it up and running.”
        author:
          name: Daniel Widerin
          title: Head of Software Delivery, Hilti
        metrics:
          - value: 400%
            text: Increase in code checks (increase from 6 to 24 code checks every three months)
          - value: 50%
            text: Shorter feedback loops
          - value: 15 min
            text: Deployment time down from 3 hours
        button:
          href: /customers/hilti/
      banner:
        text: Learn how GitLab for Enterprises can help your team innovate, modernize, and accelerate.
        href: /enterprise/
        icon:
          name: increase
          size: lg
    - name: 'Mid-market'
      cta:
        text: See more Mid-market stories
        href: /customers/all/?companySize=mid-market
      cases:
        - image: /nuxt-images/blogimages/thezebra_cover.jpg
          name: The Zebra
          text: Achieving secure pipelines in black and white
          button:
            href: /customers/thezebra/
        - image: /nuxt-images/blogimages/conversicaimage.jpg
          name: Conversica
          text: Leading AI innovation with help from GitLab Ultimate
          button:
            href: /customers/conversica/
        - image: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
          name: Moneyfarm
          text: Deploying faster using fewer tools
          button:
            href: /customers/moneyfarm/
      quote:
        header: HackerOne achieves 5x faster deployments with GitLab’s integrated security
        logo: /nuxt-images/logos/hackerone-logo.png
        text: “GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.”
        author:
          name: Mitch Trale
          title: Head of Infrastructure, HackerOne
        metrics:
          - value: 7.5x
            text: Faster pipeline time
          - value: 5x
            text: Faster deployment time
          - value: 4 hours
            text: Saved weekly per engineer
        button:
          href: /customers/hackerone/
      banner:
        text: Learn how GitLab for Enterprises can help your team innovate, modernize, and accelerate.
        href: /enterprise/
        icon:
          name: increase
          size: lg
    - name: 'Small Business'
      cta:
        text: See more Small business stories
        href: /customers/all/?companySize=small-business
      cases:
        - image: /nuxt-images/blogimages/glympse_case_study.jpg
          name: Glympse
          text: Making geo-location sharing easy with GitLab
          button:
            href: /customers/glympse/
        - image: /nuxt-images/blogimages/nebulaworks.jpg
          name: Nebulaworks
          text: Boosting customer speed and agility with fewer tools
          button:
            href: /customers/nebulaworks/
        - image: /nuxt-images/blogimages/remote.jpg
          name: Remote
          text: How Remote meets 100% of deadlines with GitLab
          button:
            href: /customers/remote/
      quote:
        header: FullSave uses GitLab to reduce DevOps toolchain and dramatically multiply deployments
        logo: /nuxt-images/case-study-logos/fullsave-logo.png
        text: “GitLab is an all-in-one solution that offers clarity and helps to improve the whole team’s efficiency.”
        author:
          name: Laurent Lavallade
          title: Chief Technology Officer, FullSave
        metrics:
          - value: 95%
            text: Decrease in deployment time
          - value: 12x
            text: Increase in deployment frequency
        button:
          href: /customers/fullsave/
      banner:
        text: Learn how GitLab for Enterprises can help your team innovate, modernize, and accelerate.
        href: /enterprise/
        icon:
          name: increase
          size: lg
badges:
    variant: light
    header: Loved by users. Recognized by analysts.
    tabs :
      - tab_name: G2 Leader in DevSecOps
        tab_id: leaders
        tab_icon:  /nuxt-images/icons/ribbon-check-transparent.svg
        copy: |
          GitLab ranks as a G2 Leader across DevSecOps categories
        badge_images:
          - src: /nuxt-images/badges/enterpriseleader_fall2022.svg
            alt: G2 Enterprise Leader - Fall 2022
          - src: /nuxt-images/badges/midmarketleader_fall2022.svg
            alt: G2 Mid-Market Leader - Fall 2022
          - src: /nuxt-images/badges/smallbusinessleader_fall2022.svg
            alt: G2 Small Business Leader - Fall 2022
          - src: /nuxt-images/badges/bestresults_fall2022.svg
            alt: G2 Best Results - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipenterprise_fall2022.svg
            alt: G2 Best Relationship Enterprise - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipmidmarket_fall2022.svg
            alt: G2 Best Relationship Mid-Market - Fall 2022
          - src: /nuxt-images/badges/easiesttodobusinesswith_fall2022.svg
            alt: G2 Easiest To Do Business With Mid-Market - Fall 2022
          - src: /nuxt-images/badges/bestusability_fall2022.svg
            alt: G2 Best Usability - Fall 2022
      - tab_name: Industry Analyst Research
        tab_id: research
        tab_icon: /nuxt-images/icons/doc-pencil-transparent.svg
        copy: |
          What top Industry Analysts are saying about GitLab
        cta:
          url: /analysts
          data_ga_name: analysts page
          data_ga_location: industry analyst research tab - badge section
        analysts:
          - logo: /nuxt-images/logos/forrester-logo.svg
            text: 'The 2019 Forrester Wave™: Cloud-Native Continuous Integration Tools'
            link:
              url: /analysts/forrester-cloudci19/
              data_ga_name: Forrester Cloud-Native Continuous Integration Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant for Enterprise Agile Planning Tools'
            link:
              url: /analysts/gartner-eapt21/
              data_ga_name: Gartner Magic Quadrant for Enterprise Agile Planning Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant™ for Application Security Testing'
            link:
              url: /analysts/gartner-ast22/
              data_ga_name: Gartner Magic Quadrant for Application Security Testing
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2021 Gartner® Market Guide for Value Stream Delivery Platforms'
            link:
              url: /analysts/gartner-vsdp21/
              data_ga_name: Gartner Market Guide for Value Stream Delivery Platforms
link:
  header: Learn how GitLab solves some of today’s toughest challenges
  heading_variant: heading4-bold
  image_left: true
  purple_background: true
  button:
    href: /customers/all/
    text: Browse all customer stories
  image: /nuxt-images/blogimages/customers-link.jpg
  alt: crowd during a presentation
  icon: calendar-alt-2
